package com.cgi.jira.soap.endpoints;

import com.cgi.jira.service.TicketService;
import com.cgi.jira.ws.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class TicketEndPoint {
    private static final String NAMESPACE_URI = "http://jira.cgi.com/ws";

    private TicketService ticketService;

    @Autowired
    public TicketEndPoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketRequest")
    @ResponsePayload
    public GetTicketResponse getTicket(@RequestPayload GetTicketRequest request) {
        GetTicketResponse getTicketResponse = new GetTicketResponse();
        getTicketResponse.setTicketSoap(ticketService.getTicket(request));
        return getTicketResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "InsertNewTicketRequest")
    @ResponsePayload
    public InsertNewTicketResponse insertNewTicket(@RequestPayload InsertNewTicketRequest request) {
        NewTicket newTicket = request.getNewTicket();
        Long id = ticketService.insertTicket(newTicket);
        InsertNewTicketResponse insertNewTicketResponse = new InsertNewTicketResponse();
        insertNewTicketResponse.setNewTicket(ticketService.getTicketById(id));
        return  insertNewTicketResponse;
    }
}
