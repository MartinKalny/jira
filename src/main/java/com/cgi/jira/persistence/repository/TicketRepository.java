package com.cgi.jira.persistence.repository;

import com.cgi.jira.persistence.entities.Ticket;
import com.cgi.jira.persistence.mappers.TicketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.Arrays;

@Repository
public class TicketRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Ticket getTicketById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id=?",
                new Object[]{id}, new TicketMapper());
    }


    public Ticket getTicketByName(String name) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE name=?",
                new Object[]{name}, new TicketMapper());
    }

    public Long insertTicket(Ticket ticket) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO ticket (name, id_person_creator, id_person_assigned, creation_datetime, " +
                "ticket_close_datetime) VALUES (?, ?, ?, ?, ?) ";

        Timestamp creationDateTime;
        if(ticket.getCreationDateTime() != null){
            creationDateTime = Timestamp.valueOf(ticket.getCreationDateTime());
        }else{
            creationDateTime = null;
        }
        Timestamp ticketCloseDateTime;
        if(ticket.getTicketCloseDateTime() != null){
            ticketCloseDateTime = Timestamp.valueOf(ticket.getTicketCloseDateTime());
        }else{
            ticketCloseDateTime = null;
        }

        PreparedStatementCreatorFactory creatorFactory = new PreparedStatementCreatorFactory(sql, Types.VARCHAR,
                Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP);
        creatorFactory.setReturnGeneratedKeys(true);
        PreparedStatementCreator creator = creatorFactory.newPreparedStatementCreator(Arrays.asList(ticket.getName(),
                ticket.getIdPersonCreator(), ticket.getIdPersonAssigned(), creationDateTime, ticketCloseDateTime));
        jdbcTemplate.update(creator, keyHolder);
        return (Long) keyHolder.getKeys().get("id");

    }

}

