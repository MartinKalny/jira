package com.cgi.jira.service;

import com.cgi.jira.persistence.entities.Ticket;
import com.cgi.jira.persistence.repository.TicketRepository;
import com.cgi.jira.ws.GetTicketRequest;
import com.cgi.jira.ws.NewTicket;
import com.cgi.jira.ws.TicketSoap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TicketService {
    private TicketRepository ticketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

   public TicketSoap getTicket(GetTicketRequest request){
        if(request.getName() == null && request.getId() == null){
            throw new RuntimeException("No requestors found.");
        }else if(request.getId() != null && request.getName() == null){
            return getTicketById(request.getId());
        }else if(request.getName() != null && request.getId() == null){
            return getTicketByName(request.getName());
        }else{
            throw new RuntimeException("Too many requestors. Choose one.");
        }
   }

    public TicketSoap getTicketByName(String name){
        Ticket ticket = ticketRepository.getTicketByName(name);
        return mapTicketToTicketSoap(ticket);
    }

    public TicketSoap getTicketById(Long id){
        Ticket ticket = ticketRepository.getTicketById(id);
        return mapTicketToTicketSoap(ticket);
    }

    public Long insertTicket(NewTicket newTicket){
        return ticketRepository.insertTicket(mapNewTicketToTicket(newTicket));
    }

    private TicketSoap mapTicketToTicketSoap(Ticket ticket) {
        TicketSoap ticketSoap = new TicketSoap();
        ticketSoap.setId(ticket.getId());
        ticketSoap.setName(ticket.getName());
        ticketSoap.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketSoap.setIdPersonAssigned(ticket.getIdPersonAssigned());
        ticketSoap.setCreationDateTime(ticket.getCreationDateTime());
        ticketSoap.setTicketCloseDateTime(ticket.getTicketCloseDateTime());

        return ticketSoap;
    }

    private Ticket mapNewTicketToTicket(NewTicket newTicket) {
        Ticket ticket = new Ticket();
        ticket.setName(newTicket.getName());
        ticket.setIdPersonCreator(newTicket.getIdPersonCreator());
        ticket.setIdPersonAssigned(newTicket.getIdPersonAssigned());
        ticket.setCreationDateTime(newTicket.getCreationDateTime());
        ticket.setTicketCloseDateTime(newTicket.getTicketCloseDateTime());

        return ticket;
    }
}
