package com.cgi.jira.persistence.entities;

import java.time.LocalDateTime;

public class Ticket {
    private Long id;
    private String name;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    private LocalDateTime creationDateTime;
    private LocalDateTime ticketCloseDateTime;


    public Ticket() {
    }

    public Ticket(Long id, String name, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime, LocalDateTime ticketCloseDateTime) {
        this.id = id;
        this.name = name;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
        this.ticketCloseDateTime = ticketCloseDateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public LocalDateTime getTicketCloseDateTime() {
        return ticketCloseDateTime;
    }

    public void setTicketCloseDateTime(LocalDateTime ticketCloseDateTime) {
        this.ticketCloseDateTime = ticketCloseDateTime;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDateTime=" + creationDateTime +
                ", ticketCloseDateTime=" + ticketCloseDateTime +
                '}';
    }
}
